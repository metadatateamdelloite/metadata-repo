declare module "@salesforce/apex/CosminController.getAccountsAndContacts" {
  export default function getAccountsAndContacts(): Promise<any>;
}
