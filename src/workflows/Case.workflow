<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_VP_about_cases_for_Large_Accounts</fullName>
        <ccEmails>georgethemircescu@gmail.com</ccEmails>
        <description>Notify VP about cases for Large Accounts</description>
        <protected>false</protected>
        <recipients>
            <recipient>VP_of_Services</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SUPPORT_New_case_for_large_account</template>
    </alerts>
    <rules>
        <fullName>New Case at Large Account Notification</fullName>
        <actions>
            <name>Notify_VP_about_cases_for_Large_Accounts</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Account.NumberOfEmployees</field>
            <operation>greaterThan</operation>
            <value>5000</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.AnnualRevenue</field>
            <operation>greaterThan</operation>
            <value>&quot;USD 20,000,000&quot;</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Touch_base_with_customer_regarding_case</name>
                <type>Task</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>Touch_base_with_customer_regarding_case</fullName>
        <assignedToType>accountOwner</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.ClosedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Touch base with customer regarding case</subject>
    </tasks>
</Workflow>
