public with sharing class CosminController {
    @AuraEnabled
    public static List<Account> getAccountsAndContacts(){
        List<Account> accounts = [SELECT Id, Name, (SELECT Id, Name FROM Contacts) FROM Account LIMIT 5];
        return accounts;
    }
}