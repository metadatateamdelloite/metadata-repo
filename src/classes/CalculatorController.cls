public class CalculatorController {
    public String selectedOperator{
        get;
        set;
    }
    public List<SelectOption> operators {
        get;
        set;
    }

    public Integer firstNumber {
        get;set;
    }

    public Integer secondNumber {
        get;set;
    }

    public Double calculatedResult {
        get;set;
    }
    
    public CalculatorController() {
        operators = new List<SelectOption>();
        operators.add(new SelectOption('1', '+'));
        operators.add(new SelectOption('2', '-'));
        operators.add(new SelectOption('3', '*'));
        operators.add(new SelectOption('4', '/'));
    }

    public void calculate() {
        if (selectedOperator == '1') {
            calculatedResult = firstNumber + secondNumber;
        } else if (selectedOperator == '2') {
            calculatedResult = firstNumber - secondNumber;
        } else if (selectedOperator == '3') {
            calculatedResult = firstNumber * secondNumber;
        } else if (selectedOperator == '4') {
            calculatedResult = firstNumber / secondNumber;
        }
    }

}