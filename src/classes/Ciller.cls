public with sharing class Ciller {
    public Ciller() {
        
    }

    public static List<Opportunity> getOpportunities(){
        List<Opportunity> listOfOpportunities = [SELECT ID, Name, Amount FROM Opportunities LIMIT 5];
        return listOfOpportunities;
    }
}
