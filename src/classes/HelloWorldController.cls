public class HelloWorldController {
    public String helloMessage{
        get;
        set;
    }

    public helloWorldController() {
        this.helloMessage = 'Hello from code';
    }

    public void changeTheMessage() {
        this.helloMessage = 'Hello again!' + UserInfo.getFirstName();
    }
}