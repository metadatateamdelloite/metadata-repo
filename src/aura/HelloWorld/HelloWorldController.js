({
    sayHelloWorld : function(component, event, helper) {
        var welcomeMessage = component.get("v.welcomeMessage");
        
        component.set("v.welcomeMessage", "I am a dynamic message");
        
        console.log(welcomeMessage);
    }
})