({
    onInit: function (component, event, helper) {
        console.log("Cosmin");
        var action = component.get('c.getAccountsAndContacts');
        action.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
                var abc = response.getReturnValue();
                console.log("Cosmin succes abc", abc[0].Contacts);
                component.set('v.contactsAndAccounts', abc);
            } else {
                let errors = response.getError();
                let errorMessage = errors && errors.length > 0 ? errors[0].message : 'Unknown error occurred';
                console.log(errorMessage)
            }
        });
        $A.enqueueAction(action);
    }
})